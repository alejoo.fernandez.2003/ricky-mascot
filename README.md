# Ricky Rocky Linux mascot

Source code of Ricky Rocky linux mascot SVG.

---

![Ricky Rocky Linux mascot|353x500](Ricky_Rocky_Linux_mascot_raster.png)

# Attribution

"cc-licenses" by santisoler is licensed under Creative Commons Attribution 4.0 International License. https://creativecommons.org/licenses/by/4.0/
